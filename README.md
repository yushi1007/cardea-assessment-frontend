# Welcome to Cardea

### The following steps should help you get the server up and running:

- Please clone this repository to run the server http://localhost:3000/
- $ cd into the directory folder "cardea-assessment-frontend"
- npm install and yarn install
- npm start or yarn start 
- Make sure the backend server is running
- Please go to http://127.0.0.1:8000/graphql if you want to see the data

Please let me know if it's not working for you

My email: yushiys95@gmail.com