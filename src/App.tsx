import React from 'react';
import './App.css';
import ListPage from './containers/ListPage';

function App() {
  return (
    <div className="App">
        <ListPage />
    </div>
  );
}

export default App;
