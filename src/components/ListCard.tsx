import React, { useState } from 'react'
import JobCard from './JobCard'
import PersonIcon from '@material-ui/icons/Person';

type Props = {
        id: string
        name: string
        description: string
        imageUrl: string
        listCurator: string
        jobs:{
            id: string
            jobTitle: string
            company: string
            location: string
            salaryMin: Uint8Array
            salaryMax: Uint8Array
            applyLink: string
            tag: string
        }[]
}

function ListCard({ name, description, imageUrl, listCurator, jobs }: Props) {
    const [showJobs, setShowJobs] = useState(false)

    const handleJobClick = () => {
        setShowJobs(showJobs => !showJobs)
    }

    const jobItems = jobs.map(job => {
        return(
            <JobCard 
                key={job.id}
                id={job.id}
                jobTitle={job.jobTitle}
                company={job.company}
                location={job.location}
                salaryMin={job.salaryMin}
                salaryMax={job.salaryMax}
                applyLink={job.applyLink}
                tag={job.tag}
            />
        )
    })

    return (
    <div className="list-card-container">
        <div className="list-card">
            <div onClick={handleJobClick} className="list-content ">
                <div className="list-img">
                    <img src={imageUrl} alt='logo'/>
                </div>
                <div className="list-details">
                    <h3>{name}</h3>
                    <h4><PersonIcon className="person-icon" /> {listCurator}</h4>
                    <p>{description}</p>
                </div>
            </div>
        { showJobs && 
                <div className="job-container animations">
                    {jobItems}
                </div> 
        }
        </div>
    </div>
    )
}

export default ListCard;
