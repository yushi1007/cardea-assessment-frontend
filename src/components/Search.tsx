import React from 'react'

function Search() {
    return (
        <div className="search-bar">
            <div  className="search-form-box">
                <form className="search-form">
                    <input type="text" className="search-field business" placeholder="Job Title" />
                    <input type="text" className="search-field input-location" placeholder="Location" />
                    <button className="search-btn" type="submit">Search</button>
                </form>
            </div>
        </div>
    )
}

export default Search;
