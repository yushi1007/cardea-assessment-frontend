import React from 'react'

type Props = {
    Icon: any
    title: string
}

function SidebarOption({ Icon, title }: Props) {
    return (
        <div className="sidebaroption">
            {Icon && <Icon className="sidebaroption-icon" />}
            {Icon ? <h4>{title}</h4> : <p>{title}</p>}
        </div>
    )
}

export default SidebarOption;
