import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

function Filter() {
    return (
        <div className="filter-section">
            <div className="filter-content">
                <div className="salary-section">
                    <FormLabel><span style={{ fontSize: '12px' }}>Salary range</span></FormLabel>
                    <FormGroup>
                        <FormControlLabel
                            control={<Checkbox 
                                style={{transform: "scale(0.8)",}} 
                                color="primary" 
                                />}
                            label={<span style={{ fontSize: '12px' }}>$40,000 - $60,000</span>}
                        />
                        <FormControlLabel
                            control={<Checkbox style={{transform: "scale(0.8)",}}  color="primary" />}
                            label={<span style={{ fontSize: '12px' }}>$60,000 - $80,000</span>}
                        />
                        <FormControlLabel
                            control={<Checkbox style={{transform: "scale(0.8)",}}  color="primary" />}
                            label={<span style={{ fontSize: '12px' }}>$80,000 - $100,000</span>}
                        />
                        <FormControlLabel
                            control={<Checkbox style={{transform: "scale(0.8)",}}  color="primary" />}
                            label={<span style={{ fontSize: '12px' }}>$100,000 - $120,000</span>}
                        />
                    </FormGroup>
                </div>
                <div className="location-section">
                    <FormLabel><span style={{ fontSize: '12px' }}>Location</span></FormLabel>
                        <FormGroup>
                            <FormControlLabel
                                control={<Checkbox style={{transform: "scale(0.8)",}} color="primary" />}
                                label={<span style={{ fontSize: '12px' }}>New York</span>}
                            />
                            <FormControlLabel
                                control={<Checkbox style={{transform: "scale(0.8)",}} color="primary" />}
                                label={<span style={{ fontSize: '12px' }}>New Jersey</span>}
                            />
                            <FormControlLabel
                                control={<Checkbox style={{transform: "scale(0.8)",}} color="primary" />}
                                label={<span style={{ fontSize: '12px' }}>Chicago</span>}
                            />
                            <FormControlLabel
                                control={<Checkbox style={{transform: "scale(0.8)",}} color="primary" />}
                                label={<span style={{ fontSize: '12px' }}>Los Angeles</span>}
                            />
                        </FormGroup>
                </div>
                <div className="worktype-section">
                    <FormLabel><span style={{ fontSize: '12px' }}>Work type</span></FormLabel>
                        <FormGroup>
                            <FormControlLabel
                                control={<Checkbox style={{transform: "scale(0.8)",}} color="primary" />}
                                label={<span style={{ fontSize: '12px' }}>Full-time</span>}
                            />
                            <FormControlLabel
                                control={<Checkbox style={{transform: "scale(0.8)",}} color="primary" />}
                                label={<span style={{ fontSize: '12px' }}>Part-time</span>}
                            />
                            <FormControlLabel
                                control={<Checkbox style={{transform: "scale(0.8)",}} color="primary" />}
                                label={<span style={{ fontSize: '12px' }}>Internship</span>}
                            />
                            <FormControlLabel
                                control={<Checkbox style={{transform: "scale(0.8)",}} color="primary" />}
                                label={<span style={{ fontSize: '12px' }}>Contract</span>}
                            />
                        </FormGroup>
                </div>
            </div>
        </div>
    )
}

export default Filter;
