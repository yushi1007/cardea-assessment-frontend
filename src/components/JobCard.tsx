import React, { useState } from 'react';
import PlaceIcon from '@material-ui/icons/Place';

type Props = {
        key: string
        id: string
        jobTitle: string
        company: string
        location: string
        salaryMin: Uint8Array
        salaryMax: Uint8Array
        applyLink: string
        tag: string
}

function JobCard({ jobTitle, company, location, salaryMax, salaryMin, applyLink, tag}: Props) {

    const [showDetails, setShowDetails] = useState(false)

    const handleDetailsClick = () => {
        setShowDetails(showDetails => !showDetails)
    }

    return (
        <div className="job-card-container">
            <div onClick={handleDetailsClick} className="job-card">
                <div className="job-content">
                    <h3>{company}</h3>
                    <h5>{jobTitle}</h5>
                    <div className="location">
                        <p><PlaceIcon className="place-icon" /> {location}</p>
                    </div>
                </div>
            { showDetails && 
                <div className="job-details animations">
                    <span>${salaryMin} - ${salaryMax}</span>
                    <div className="tag-link">
                        <p>{tag}</p>
                        <div className="link">
                            <a href={applyLink}>Apply Now</a>
                        </div>
                    </div>
                </div> 
            }
            </div> 
        </div>
    )
}

export default JobCard;
