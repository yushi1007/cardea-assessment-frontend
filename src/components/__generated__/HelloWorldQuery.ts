/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: HelloWorldQuery
// ====================================================

export interface HelloWorldQuery {
  hello: string | null;
}
