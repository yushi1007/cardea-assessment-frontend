import React from 'react'

function Sort() {
    return (
        <div className="sort-list">
            <select className="select-dropdown">
                <option value="Recommended">Recommended</option>
                <option value="Newest">Newest</option>
                <option value="Last Active">Last Active</option>
            </select>
        </div>
    )
}

export default Sort;
