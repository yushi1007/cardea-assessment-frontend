import React from 'react'
import Filter from '../components/Filter'
import ProfilePicture from '../images/yu.jpg'

function Profile() {
    return (
        <div className="profile-bar">
            <div className="profile-content">
                <div className="profile-img">
                    <img src={ProfilePicture} alt='profile-pic'/>
                </div>
                <div className="user-name">
                    <h3>Yu Shi</h3>
                    <span>Software Engineering</span>
                </div>
            </div>

            <br />
            <strong className="filter-title">Filter</strong>
            <hr />

            <Filter />
        </div>
    )
}

export default Profile
