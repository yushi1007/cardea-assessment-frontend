import React from 'react';
import ListCard from '../components/ListCard';
import Search from '../components/Search';
import Sort from '../components/Sort';

type Props = {
    lists: {
        id: string
        name: string
        description: string
        imageUrl: string
        listCurator: string
        jobs:{
            id: string
            jobTitle: string
            company: string
            location: string
            salaryMin: Uint8Array
            salaryMax: Uint8Array
            applyLink: string
            tag: string
        }[]
    }[]
}

function ListContainer({ lists }: Props) {

    const listItem = lists.map(list => {
        console.log(list)
        return(
            <ListCard 
                key={list.id}
                id={list.id}
                name={list.name}
                description={list.description}
                imageUrl={list.imageUrl}
                listCurator={list.listCurator}
                jobs={list.jobs}
            />
        )
    })

    return (
    <div className="list-container">
        <Search />
        <div className="list-header-container">
            <div className="list-header">
                <div className="text-content">
                    <div className="list-text">
                        <h3>Jobs For You</h3>
                        <span>Popular</span>
                    </div>
                    <div className="sort">
                        <span>sort:</span>
                        <Sort />
                    </div>
                </div>
            </div>
        </div>
        {listItem}
    </div>
    )
}

export default ListContainer;
