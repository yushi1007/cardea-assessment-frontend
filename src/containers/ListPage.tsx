import React from 'react'
import ListContainer from './ListContainer';
import { gql, useQuery } from "@apollo/client";
import SideBar from './SideBar';
import Profile from './Profile';

function ListPage() {
    const{loading, error, data}=useQuery(gql`
    {
      allLists{
        id
        name
        description
        imageUrl
        listCurator
        jobs {
          id
          jobTitle
          company
          location
          salaryMin
          salaryMax
          applyLink
          tag
        }
      }
    }
  `);

  if (loading) return <h1>Loading...</h1>
  if (error) return <h1>Error...</h1>

return (
        <div className="list-page">
          <div className="list-body">
            <SideBar />
            <ListContainer lists={data.allLists}/>
            <Profile />
          </div>
        </div>
    )
}

export default ListPage;
