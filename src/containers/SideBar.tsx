import React from 'react'
import HomeIcon from '@material-ui/icons/Home';
import SearchIcon from '@material-ui/icons/Search';
import SidebarOption from '../components/SidebarOption';
import WorkIcon from '@material-ui/icons/Work';
import EventNoteIcon from '@material-ui/icons/EventNote';
import SmsIcon from '@material-ui/icons/Sms';
import AssessmentIcon from '@material-ui/icons/Assessment';

function SideBar() {
    return (
        <div className="sidebar">
            <a href="/"><h1 className="sidebar-logo">Cardea</h1></a>
            <SidebarOption Icon={HomeIcon} title="Home" />
            <SidebarOption Icon={SearchIcon} title="Search" />
            <SidebarOption Icon={WorkIcon} title="My Jobs" />
            <SidebarOption Icon={EventNoteIcon} title="My Activity" />
            <SidebarOption Icon={SmsIcon} title="Messages" />
            <SidebarOption Icon={AssessmentIcon} title="Reports" />
        </div>
    )
}

export default SideBar;
